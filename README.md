El proyecto se ha desarrollado más o menos de esta forma:



- **Análisis**: 

    Marco fue el que propuso la idea del proyecto. La idea era una página web e-learning sobre la temática de Blockchain y otras tecnologías relacionadas con las criptomonedas, en la que se intentaría atraer a profesionales del sector para que impartieran clases.
Redactó el análisis, en base a una reunión que tuvimos, describiendo el contenido de la web (e-learning), los objetivos, las funcionalidades y las necesidades de los futuros usuarios. Se propusieron varias modificaciones entre todos. 


    El análisis encuentra [aquí](https://gitlab.com/hackablock/www.hackablock.dev/-/blob/master/Backend/Docs/analisis_funcional_hackablock.pdf)

- **Diseño**:

    Leonar desarrolló el wireframe utilizando Figma. En el wireframe se mostraban los detalles del diseño que se han intentado implementar en el proyecto final. Debido a la magnitud del proyecto que habíamos diseñado, se tuvo que recortar parte del contenido y de las funcionalidades para que encajara en el tiempo que nos daban para realizar el proyecto.

    El wireframe se encuentra [aquí](https://gitlab.com/hackablock/www.hackablock.dev/-/blob/master/Backend/Docs/wireframe_hackablock.pdf). 

    Después pasamos a diseñar la base de datos. Intentamos diseñar las tablas teniendo en cuenta varias funcionalidades, como el campo de tutor y  de administrador. Hay 2 tablas, una de usuarios y otra de cursos, con una relación N:M utilizando otra tabla llamada usuarios_cursos. Obviamente, el diseño de la base de datos no se corresponde al 100% con el resultado final.

    El diseño de la BBDD se encuentra [aquí](https://gitlab.com/hackablock/www.hackablock.dev/-/blob/master/Backend/Docs/esquema_BBDD.txt)
    
    [Diagrama de la BBDD](https://gitlab.com/hackablock/www.hackablock.dev/-/blob/master/Backend/Docs/diagramaER.png)


- **Desarrollo**:

    _Backend_

    El Backend lo desarrolló Juan Manuel, ya que dominaba un poco más la parte de la base de datos y las consultas, además de Node.js. Después de decidir cómo funcionaría la base de datos, creamos las tablas en MySQL Workbench, haciendo pequeños test comprobando que se podían agregar, modificar y borrar datos. Intentamos tener en cuenta todas las consultas que podríamos necesitar y las testeamos.

    Las consultas se encuentran en el mismo archivo que el diseño [de la BBDD](https://gitlab.com/hackablock/www.hackablock.dev/-/blob/master/Backend/Docs/esquema_BBDD.txt), hacia el final del documento.

    A continuación pasamos a Node.js, creando el servidor y testéandolo. De acuerdo al diseño, creamos los endpoints siguiendo las clases y probándolos en Postman. Como teníamos la base de datos creada (localmente) en MySQL Workbench, pudimos comprobar que los endpoints se comportaban correctamente.
Decidimos no añadir algunas de las herramientas que utilizaríamos más adelante, porque así sería más sencillo ampliar o modificar cosas en el caso de que fuera necesario

    Algunos de los endpoints no se utilizan, pero se mantuvieron por si acaso hicieran falta

    _Frontend_

    Para desarrollar el Frontend de Hackablock, decidimos que Leonar lo trabajara más ya que su experiencia previa en creación de páginas webs estáticas y diseño gráfico podrían enriquecer el proyecto.

    Para facilitarle el trabajo, le pasamos algunas páginas ya maquetadas para sólo aplicarles el estilo, el código React fue realizado por él usando cliente Axios, hooks, estados, handlebars y modales.

    A medida que se avanzaba, iban aparecieron errores  en las peticiones entre el back y el front, además de que las necesidades iban cambiando. Para ello, volvíamos a recurrir a las reuniones compartiendo pantalla, e intentábamos resolverlo.
