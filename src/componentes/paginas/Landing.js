import React, { useEffect, useState, Fragment } from 'react';
import urlAxios from '../config/axios';
import { Link } from 'react-router-dom';
import CursoLanding from '../cursos/CursoLanding';
import TutorLanding from '../tutores/TutorLanding';
import Blockchain12 from '../../images/Blockchain12-01.png';
import backbanner from '../../images/backbanner.jpeg';
import logouan from '../../images/logouan.png';
import logoue from '../../images/logoue.png';
import logouoc from '../../images/logouoc.png';
import logousc from '../../images/logousc.png';
import './Landing.css';
import '../cursos/Cursos.css';

function Landing() {
  const [cursos, guardarCursos] = useState([]);
  const [tutores, guardarTutores] = useState([]);

  useEffect(() => {
    const consultarAPIS = async () => {
      const cursosConsulta = await urlAxios.get('/api/courses/');
      guardarCursos(cursosConsulta.data);
    };
    consultarAPIS();
  }, []);

  useEffect(() => {
    const consultarAPIST = async () => {
      const tutoresConsulta = await urlAxios.get('/api/teachers');
      guardarTutores(tutoresConsulta.data);
    };
    consultarAPIST();
  }, []);

  const list = cursos;
  list.sort((a, b) => (b.valoracion > a.valoracion ? 1 : -1));

  const lists = tutores;
  lists.sort((a, b) => (b.valoracion > a.valoracion ? 1 : -1));

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <Fragment>
      <div className="contenedor-landing">
        <div className="contenedor-banner">
          <div className="info-banner">
            <h1>¡Aprende Blockchain!</h1>
          </div>
          <div className="gradiente"></div>
          <img src={backbanner} alt="" />
        </div>
        <div className="publi-1">
          La demanda del profesional del Blockchain está en auge y se espera que
          la demanda de esta tecnología crezca en más del 60% por año.
        </div>
        <div className="block-1">
          <img src={Blockchain12} className="img-2" alt="" />
        </div>
        <div className="banner-frase">
          <div className="frase-texto">
            " Blockchain es el desafío financiero de nuestro tiempo.<br></br>
            Va a cambiar la forma en que opera nustro mundo. "
            <div className="frase-autor">Lythe Masters</div>
          </div>
        </div>
        <div className="contenedor-cursos-landing">
          <div>
            <p className="ultimos-cursos-landing">TOP CURSOS</p>
          </div>
          <ul className="contenido-cursos-landing">
            {cursos.slice(0, 6).map((curso) => (
              <CursoLanding key={curso.id} curso={curso} />
            ))}
          </ul>
          <Link to="/cursos">
            <button className="ver-cursos">VER CURSOS</button>
          </Link>
        </div>
        <div className="banner-frase-2">
          <div className="frase-texto-2">
            <strong>350</strong>
            <p>Estudiantes</p>
          </div>
          <div className="frase-texto-2">
            <strong>25</strong>
            <p>Cursos</p>
          </div>
          <div className="frase-texto-2">
            <strong>12</strong>
            <p>Tutores</p>
          </div>
        </div>
        <div className="contenedor-tutores-landing">
          <div>
            <p className="ultimos-tutores-landing">TOP TUTORES</p>
          </div>
          <ul>
            {tutores.slice(0, 3).map((tutor) => (
              <TutorLanding key={tutor.id} tutor={tutor} />
            ))}
          </ul>
        </div>
        <Link to="/tutores">
          <button className="ver-tutores">VER TUTORES</button>
        </Link>
        <div className="banner-confian-logos">
          <div>
            <p className="confian-nosotros">CONFÍAN EN NOSOTROS</p>
          </div>
          <div className="banner-logos">
            <img src={logouan} className="logo" alt="" />
            <img src={logoue} className="logo" alt="" />
            <img src={logouoc} className="logo" alt="" />
            <img src={logousc} className="logo" alt="" />
          </div>
        </div>
        <div className="contenedor-testimonial">
          <div className="imagen-testimonial">
            <div className="img-testimonial-back">
              <div className="img-testimonial-front"></div>
            </div>
          </div>
          <div className="testimonial">
            <i className="fas fa-quote-left"></i>
            <p>
              Los cursos son muy didácticos y tienen mucho valor agregado en sus
              contenidos. Y los tutores son muy profesionales además siempre te
              dan buenos concejos.
            </p>
            <div className="contenedor-nombre-avatar">
              <div className="nombre-testimonial">
                <strong>Alejandra</strong>
                <p>Empresaria</p>
              </div>
              <div className="img-avatar-testimonial"></div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default Landing;
