import React from 'react';
import CursoInfo from './CursoInfo';
import './CursoFloat.css';

function CursoFloat({ curso }) {
  const {
    tutor,
    titulo,
    avatar,
    imagen,
    nivel,
    valoracion,
    valor,
    precio,
    n_visitas,
    comentario,
    id,
  } = curso;

  return (
    <a href={`/cursos/${id}`} onClick={() => CursoInfo(id)}>
      <div className="entrada-float">
        {valor === 'gratis' ? (
          <div className="gratis">
            <span>{valor}</span>
          </div>
        ) : null}
        <div className="imagen-curso">
          <img src={`http://localhost:5000/${imagen}`} alt="" />
        </div>

        <div className="avatar-curso">
          <img src={`http://localhost:5000/${avatar}`} alt="" />
        </div>
        <div className="tutor-curso">
          <p>{tutor}</p>
        </div>
        <div className="titulo-curso">
          <h3>{titulo}</h3>
        </div>
        <div className="linea-horizontal"></div>
        <div className="nivel-valoracion">
          <div className="nivel">
            <p>{nivel}</p>
          </div>
          <div className="valoracion-curso">
            {Array.from({ length: valoracion }, (_, i) => (
              <i key={i} className="fa fa-star amarilla" aria-hidden="true"></i>
            ))}
          </div>
        </div>
        <div className="vistas-comentarios-precios">
          <div className="titulo-curso">
            <p>
              <i className="fas fa-users" /> {n_visitas}
            </p>
          </div>
          <div className="titulo-curso">
            <p>
              <i className="fas fa-comments" /> {comentario}
            </p>
          </div>
          {valor === 'gratis' ? (
            <div className="precio-curso-gratis">
              <p>€{precio}</p>
            </div>
          ) : (
            <div className="precio-curso">
              <p>€{precio}</p>
            </div>
          )}
        </div>
      </div>
    </a>
  );
}

export default CursoFloat;
