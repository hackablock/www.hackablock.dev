import React, { useEffect, useState, Fragment, useCallback } from 'react';
import urlAxios from '../config/axios';
import Curso from './Curso';
import './Cursos.css';

function Cursos() {
  const [cursos, guardarCursos] = useState([]);
  const [ordenar, guardarOrdenar] = useState({});
  const conditionInicial = {
    condition: '0',
  };

  const initialState = {
    nivel: '',
    valoracion: '',
    valor: '',
    categoria_blockchain: '',
    categoria_bitcoin: '',
    categoria_crypto: '',
    categoria_ethereum: '',
    categoria_hyperledger: '',
    id_tutor: '',
  };

  const [filtros, guardarfiltros] = useState(initialState);

  useEffect(() => {
    const consultarAPI = async () => {
      const cursosConsulta = await urlAxios.get('/api/courses/');
      guardarCursos(cursosConsulta.data);
    };
    consultarAPI();
  }, []);

  const filtroAPI = useCallback(
    async (e) => {
      e.preventDefault();

      const params = new URLSearchParams();
      params.append('nivel', filtros.nivel);
      params.append('valoracion', filtros.valoracion);
      params.append('valor', filtros.valor);
      params.append('categoria_blockchain', filtros.categoria_blockchain);
      params.append('categoria_bitcoin', filtros.categoria_bitcoin);
      params.append('categoria_crypto', filtros.categoria_crypto);
      params.append('categoria_ethereum', filtros.categoria_ethereum);
      params.append('categoria_hyperledger', filtros.categoria_hyperledger);
      params.append('id_tutor', filtros.id_tutor);
      const request = {
        params: params,
      };
      const cursosConsulta = await urlAxios.get('/api/courses', request);
      guardarCursos(cursosConsulta.data);

      return cursosConsulta;
    },
    [filtros]
  );

  const borrarFiltros = async () => {
    let x = document.getElementsByTagName('input');
    let i;
    for (i = 0; i < x.length; i++) {
      x[i].checked = false;
    }
    guardarfiltros(initialState);
    guardarOrdenar(conditionInicial);
  };

  const leerDatos = async (e) => {
    let a = document.getElementById('categoria_blockchain');
    let b = document.getElementById('categoria_bitcoin');
    let c = document.getElementById('categoria_crypto');
    let d = document.getElementById('categoria_ethereum');
    let f = document.getElementById('categoria_hyperledger');

    if (a.checked) {
      a.value = '1';
    } else {
      a.value = '';
    }
    if (b.checked) {
      b.value = '1';
    } else {
      b.value = '';
    }
    if (c.checked) {
      c.value = '1';
    } else {
      c.value = '';
    }
    if (d.checked) {
      d.value = '1';
    } else {
      d.value = '';
    }
    if (f.checked) {
      f.value = '1';
    } else {
      f.value = '';
    }

    guardarfiltros({
      ...filtros,
      [e.target.name]: e.target.value,
    });
  };
  useEffect(() => {}, [filtroAPI]);

  const ordenarPor = (e) => {
    guardarOrdenar({
      ...ordenar,
      [e.target.name]: e.target.value,
    });
  };

  const list = cursos;
  const x = ordenar.value;

  if (Number(x) === 0) {
    list.sort((a, b) => (a.id > b.id ? 1 : -1));
  }
  if (Number(x) === 1) {
    list.sort((a, b) => (b.fecha_creacion > a.fecha_creacion ? 1 : -1));
  }
  if (Number(x) === 2) {
    list.sort((a, b) => (b.n_visitas > a.n_visitas ? 1 : -1));
  }
  if (Number(x) === 3) {
    list.sort((a, b) => (b.valoracion > a.valoracion ? 1 : -1));
  }

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <Fragment>
      <main className="pagina-cursos">
        <div className="filtros">
          {/*<select id="nivel" name="nivel" onChange={leerDatos}>
            <option value="Fácil">Fácil</option>
            <option value="intermedio">intermedio</option>
            <option value="Experto">experto</option>
          </select>*/}
          <form id="formfiltros" onSubmit={filtroAPI}>
            <p>Nivel</p>
            <div>
              <input
                type="radio"
                name="nivel"
                value="basico"
                onChange={leerDatos}
              />
              <span> Básico</span>
            </div>
            <div>
              <input
                type="radio"
                name="nivel"
                value="intermedio"
                onChange={leerDatos}
              />
              <label> Intermedio</label>
            </div>
            <div>
              <input
                type="radio"
                name="nivel"
                value="avanzado"
                onChange={leerDatos}
              />
              <label> Avanzado</label>
            </div>

            <p>Precio</p>
            <div>
              <input
                type="radio"
                id="valor"
                name="valor"
                value="pago"
                onChange={leerDatos}
              />
              <label> Pago</label>
              <div>
                <input
                  type="radio"
                  id="valor"
                  name="valor"
                  value="gratis"
                  onChange={leerDatos}
                />
                <label> Gratuito</label>
              </div>
            </div>

            <p>Valoración</p>
            <div>
              <input
                id="valoracion"
                type="radio"
                name="valoracion"
                value="5"
                onChange={leerDatos}
              />
              <span> </span>
              <span className="fa fa-star amarilla"></span>
              <span className="fa fa-star amarilla"></span>
              <span className="fa fa-star amarilla"></span>
              <span className="fa fa-star amarilla"></span>
              <span className="fa fa-star amarilla"></span>
            </div>
            <div>
              <input
                id="valoracion"
                type="radio"
                name="valoracion"
                value="4"
                onChange={leerDatos}
              />
              <span> </span>
              <span className="fa fa-star amarilla"></span>
              <span className="fa fa-star amarilla"></span>
              <span className="fa fa-star amarilla"></span>
              <span className="fa fa-star amarilla"></span>
              <span className="far fa-star"></span>
            </div>
            <div>
              <input
                id="valoracion"
                type="radio"
                name="valoracion"
                value="3"
                onChange={leerDatos}
              />
              <span> </span>
              <span className="fa fa-star amarilla"></span>
              <span className="fa fa-star amarilla"></span>
              <span className="fa fa-star amarilla"></span>
              <span className="far fa-star"></span>
              <span className="far fa-star"></span>
            </div>
            <div>
              <input
                id="valoracion"
                type="radio"
                name="valoracion"
                value="2"
                onChange={leerDatos}
              />
              <span> </span>
              <span className="fa fa-star amarilla"></span>
              <span className="fa fa-star amarilla"></span>
              <span className="far fa-star"></span>
              <span className="far fa-star"></span>
              <span className="far fa-star"></span>
            </div>
            <div>
              <input
                id="valoracion"
                type="radio"
                name="valoracion"
                value="1"
                onChange={leerDatos}
              />
              <span> </span>
              <span className="fa fa-star amarilla"></span>
              <span className="far fa-star"></span>
              <span className="far fa-star"></span>
              <span className="far fa-star"></span>
              <span className="far fa-star"></span>
            </div>
            <p>Categorías</p>
            <div>
              <input
                id="categoria_blockchain"
                type="checkbox"
                value="1"
                name="categoria_blockchain"
                onChange={leerDatos}
              />
              <span> Blockchain</span>
            </div>
            <div>
              <input
                id="categoria_bitcoin"
                type="checkbox"
                name="categoria_bitcoin"
                value="1"
                onChange={leerDatos}
              />
              <span> Bitcoin</span>
            </div>
            <div>
              <input
                id="categoria_crypto"
                type="checkbox"
                name="categoria_crypto"
                value="1"
                onChange={leerDatos}
              />
              <span> Crypto</span>
            </div>
            <div>
              <input
                id="categoria_ethereum"
                type="checkbox"
                name="categoria_ethereum"
                value="1"
                onChange={leerDatos}
              />
              <span> Ethereum</span>
            </div>
            <div>
              <input
                id="categoria_hyperledger"
                type="checkbox"
                name="categoria_hyperledger"
                value="1"
                onChange={leerDatos}
              />
              <span> Hyperledger</span>
            </div>
            <br></br>
            <p>Tutores</p>

            <div>
              <input
                type="radio"
                name="id_tutor"
                value=""
                onChange={leerDatos}
              />
              <span> Todos</span>
            </div>
            <div>
              <input
                type="radio"
                name="id_tutor"
                value="1"
                onChange={leerDatos}
              />
              <span> Juan Perez</span>
            </div>
            <div>
              <input
                type="radio"
                name="id_tutor"
                value="7"
                onChange={leerDatos}
              />
              <span> Elizabeth Merino</span>
            </div>
            <div>
              <input
                type="radio"
                name="id_tutor"
                value="4"
                onChange={leerDatos}
              />
              <span> Miguel Isaías</span>
            </div>
            <div>
              <input
                type="radio"
                name="id_tutor"
                value="2"
                onChange={leerDatos}
              />
              <span> Cristian Anastacio</span>
            </div>
            <div>
              <input
                type="radio"
                name="id_tutor"
                value="6"
                onChange={leerDatos}
              />
              <span> Jhonny Lee</span>
            </div>
            <div>
              <input
                type="radio"
                name="id_tutor"
                value="3"
                onChange={leerDatos}
              />
              <span> Esteban Ortega</span>
            </div>
            <div>
              <input
                type="radio"
                name="id_tutor"
                value="5"
                onChange={leerDatos}
              />
              <span> Melissa Brown</span>
            </div>
            <br></br>
            <div>
              <button
                type="submit"
                className="boton-aplicar-filtros"
                value="aplicarFiltros"
              >
                APLICAR FILTROS
              </button>

              <button
                type="submit"
                className="boton-quitar-filtros"
                value="borrarfiltros"
                onClick={borrarFiltros}
              >
                BORRAR FILTROS
              </button>
            </div>
          </form>
        </div>
        <div className="cursos-filtros-top">
          <div className="filtros-top">
            <div className="ordenar-por">
              <p>Ordenar por</p>
              <select name="value" onChange={ordenarPor}>
                Seleccionar:
                <option name="value" value="0">
                  Seleccionar:
                </option>
                <option name="value" value="1">
                  Más Recientes
                </option>
                <option name="value" value="2">
                  Más populares
                </option>
                <option name="value" value="3">
                  Mejor valorados
                </option>
              </select>
            </div>
            {/*             <div className="buscador">
              <form action="/action_page.php">
                <input type="text" placeholder="Search.." name="search" />
                <button type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </form>
            </div> */}
          </div>
          {cursos.length === 0 ? (
            <div id="message" className="container-alerta-resultados">
              <div>
                <i className="fas fa-exclamation-circle"></i>
              </div>
              <br></br>
              <p>
                Lo sentimos, no tenemos resultados, <br></br>por favor prueba
                con otros filtros.
              </p>
            </div>
          ) : null}
          <ul className="contenido-cursos">
            {cursos.map((curso) => (
              <Curso key={curso.id} curso={curso} />
            ))}
          </ul>
        </div>
      </main>
    </Fragment>
  );
}

export default Cursos;
