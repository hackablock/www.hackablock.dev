import urlAxios from '../config/axios';
import React, { useEffect, useState, Fragment } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import CursoFloat from './CursoFloat';
import { useParams } from 'react-router';

import './CursoInfo.css';

function CursoInfo() {
  const { id } = useParams();
  console.log(id);

  const [curso, guardarCurso] = useState({});
  const [cursos, guardarCursos] = useState([]);
  const [fecha, guardarFecha] = useState([]);

  const consultarAPI = async () => {
    const cursoConsulta = await urlAxios.get(`/api/courses/${id}`);
    guardarCurso(cursoConsulta.data);
    console.log(cursoConsulta.data);

    const resultado = cursoConsulta.data;
    const fecha_creacion = resultado.fecha_creacion;
    const fecha_slice = fecha_creacion.slice(0, 10);
    guardarFecha(fecha_slice);
  };

  useEffect(() => {
    consultarAPI();
  }, [id]);

  useEffect(() => {
    const consultarAPIS = async () => {
      const cursosConsulta = await urlAxios.get('/api/courses/');
      guardarCursos(cursosConsulta.data);
    };
    consultarAPIS();
  }, []);

  const {
    tutor,
    titulo,
    avatar,
    imagen,
    nivel,
    valoracion,
    valor,
    precio,
    n_visitas,
    categoria,
    comentario,
    duracion,
  } = curso;

  const fecha_slice = fecha;

  const list = cursos;

  list.sort((a, b) => (b.fecha_creacion > a.fecha_creacion ? 1 : -1));

  return (
    <Fragment>
      <article className="container-info">
        <div className="info-left">
          <div className="titulo-info">
            <h3>{titulo}</h3>
          </div>
          <div className="info-top">
            <div className="contenedor-avatar-tutor">
              <div className="avatar-info">
                <img src={`http://localhost:5000/${avatar}`} alt="" />
              </div>
              <div className="tutor-info">
                <span>Tutor:</span>
                <br></br>
                <p>{tutor}</p>
              </div>
            </div>
            <div className="linea-horizontal-info"></div>
            <div className="categoria-info">
              <span>Categoría:</span>
              <br></br>
              <p>{categoria}</p>
            </div>
            <div className="linea-horizontal-info"></div>
            <div className="valoracion-info">
              <span>Valoración:</span>
              <br></br>
              {Array.from({ length: valoracion }, (_, i) => (
                <i
                  key={i}
                  className="fa fa-star amarilla-info"
                  aria-hidden="true"
                ></i>
              ))}
            </div>
            <div className="linea-horizontal-info"></div>
            <div className="nivel-info">
              <span>Nivel:</span>
              <br></br>
              <p>{nivel}</p>
            </div>
            <div className="linea-horizontal-info"></div>
            {valor === 'gratis' ? (
              <div className="gratis-info">
                <span>{valor}</span>
              </div>
            ) : null}
            {valor === 'gratis' ? (
              <div className="linea-horizontal-info"></div>
            ) : null}

            {valor === 'gratis' ? (
              <div className="precio-curso-gratis-info">
                <p>€{precio}</p>
              </div>
            ) : (
              <div className="precio-curso-info">
                <p>€{precio}</p>
              </div>
            )}
          </div>

          <div className="container-imagen-info-bottom">
            <div className="imagen-borde">
              <div className="imagen-curso-info">
                <img src={`http://localhost:5000/${imagen}`} alt="" />
              </div>
            </div>
            <Tabs className="tabs">
              <TabList>
                <Tab>Información</Tab>
                <Tab>Contenido</Tab>
              </TabList>

              <TabPanel className="tab-informacion">
                <div className="desc-cert-salid">
                  <div>
                    <h1>Descripción del Curso</h1>
                    <p>
                      Lorem Ipsum has been the industry's standard dummy text
                      ever since the 1500s, when an unknown printer took a
                      galley of type and scrambled it to make a type specimen
                      book. It has survived not only five centuries, but also
                      the leap into electronic typesetting, remaining
                      essentially unchanged. It was popularised in the 1960s
                      with the release of Letraset sheets containing Lorem Ipsum
                      passages, and more recently with desktop publishing
                      software like Aldus PageMaker including versions of Lorem
                      Ipsum.
                    </p>
                  </div>
                  <hr></hr>
                  <div>
                    <h1>Certificación</h1>
                    <p>
                      Lorem Ipsum has been the industry's standard dummy text
                      ever since the 1500s, when an unknown printer took a
                      galley of type and scrambled it to make a type specimen
                      book. It has survived not only five centuries, but also
                      the leap into electronic typesetting.
                    </p>
                  </div>
                  <hr></hr>
                  <div>
                    <h1>Salidas Laborales</h1>
                    <ul>
                      <li>Desarrollador</li>
                      <li>developer</li>
                      <li>Full Stack</li>
                    </ul>
                  </div>
                </div>
                <div className="datos-curso">
                  <br></br>
                  Creado:<p>{fecha_slice}</p>
                  <br></br>
                  Estudiantes: <p>{n_visitas}</p>
                  <br></br>
                  Duración:<p>{duracion} horas</p>
                  <br></br>
                  Comentarios: <p>{comentario}</p>
                  <br></br>
                  Evaluaciones: <p>123</p>
                  <br></br>
                  Certificados: <p>78</p>
                  <br></br>
                </div>
              </TabPanel>
              <TabPanel>
                <div>
                  <h1>Contenido del curso</h1>
                  <ul>
                    <li>Html y Css</li>
                    <li>Javascript</li>
                    <li>Python</li>
                    <li>C++</li>
                    <li>Maachine Learning</li>
                    <li>React Js</li>
                    <li>React Native</li>
                    <li>Angular</li>
                    <li>Git y Gitlab</li>
                    <li>Scrum</li>
                    <li>Desarrollo Personal</li>
                    <li>Proyecto</li>
                    <li>RRSS</li>
                    <li>Emprendimiento</li>
                    <li>Entrevista</li>
                  </ul>
                </div>
              </TabPanel>
            </Tabs>
          </div>
        </div>
        <div className="info-right-float">
          <div>
            <p className="ultimos-cursos">Últimos Cursos</p>
          </div>
          <ul className="contenido-cursos-float">
            {cursos.slice(0, 4).map((curso) => (
              <CursoFloat key={curso.id} curso={curso} />
            ))}
          </ul>
        </div>
      </article>
    </Fragment>
  );
}

export default CursoInfo;
