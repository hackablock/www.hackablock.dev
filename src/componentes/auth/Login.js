import React, { useState, useContext, Fragment } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { CRMContext } from '../../context/CRMContext';
import urlAxios from '../config/axios';
import Modal from '@material-ui/core/Modal';
import jwt_decode from 'jwt-decode';

import './Login.css';

// MODAL PRESET //
function getModalStyle() {
  const top = 44;
  const left = 48.5;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 450,
    height: 200,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[0],
    padding: theme.spacing(2, 4, 3),
  },
}));

// LOGIN //
function Login(props) {
  // States, constantes y handles del Modal //
  const [modalStyle] = useState(getModalStyle);
  const [open, setOpen] = useState(false);

  const classes = useStyles();

  const handleClose = () => {
    setOpen(false);
  };

  // States //
  // Autenticacion, password, error y credenciales.
  const [auth, guardarAuth] = useContext(CRMContext);
  const [mostrarPass, setMostrarPass] = useState(false);
  const [error, setError] = useState(null);
  const [credenciales, guardarCredenciales] = useState({});

  // Función mostrar la contraseña
  const MostrarPassword = () => {
    setMostrarPass(mostrarPass ? false : true);
  };

  // iniciar sesión en el servidor
  const iniciarSesion = async (e) => {
    e.preventDefault();
    setError(null);

    // autenticar al usuario con las credenciales
    try {
      const respuesta = await urlAxios.post('api/users/login', credenciales);

      // extraer el token, userId y role del usuario, luego ubicarlo en localstorage
      const { token } = respuesta.data;
      const decoded = jwt_decode(token);
      localStorage.setItem('token', token);
      localStorage.setItem('userId', decoded.id);
      localStorage.setItem('role', decoded.role);

      //Almacenar en el state la autenticacion y el token.
      guardarAuth({
        token,
        auth: true,
      });

      //Abrir el Modal
      setOpen(true);
    } catch (error) {
      //Guardar error en el State
      setError(error.response.data.error);
    }
  };

  // Almacenar lo que el usuario escribe en el state
  const leerDatos = (e) => {
    guardarCredenciales({
      ...credenciales,
      [e.target.name]: e.target.value,
    });

    // Ver lo que el usuario escribe en la consola
    
  };

  // Handle para cerrar ventana del error
  const cerrarError = () => {
    setError(null);
  };

  const aLogin = () => {
    props.history.push('/');
  };

  return (
    <Fragment>
      {error && (
        <div id="message" className="container-alerta-login">
          {error}
          <i className="far fa-window-close" onClick={cerrarError}></i>
        </div>
      )}
      <Modal open={open} onClose={() => handleClose()}>
        <div style={modalStyle} className={classes.paper}>
          <div className="creado">
            <i className="fas fa-check-circle"></i>
            <h1>Login exitoso</h1>
            <Link to={'/'} className="boton-modal">
              Continuar
            </Link>
          </div>
        </div>
      </Modal>
      <div className="container-login">
        <form onSubmit={iniciarSesion}>
          <div className="titulo-form">
            ¡Inicia sesión con tu cuenta de HACKABLOCK!
          </div>
          <div className="fontuser">
            <input
              type="text"
              placeholder="Correo electrónico"
              name="email"
              onChange={leerDatos}
            />
            <i className="fas fa-envelope"></i>
          </div>
          <div className="font-password">
            <input
              type={mostrarPass ? 'text' : 'password'}
              name="password"
              placeholder="Contraseña"
              onChange={leerDatos}
            />
            <i className="fa fa-key"></i>
            {mostrarPass === false ? (
              <i onClick={MostrarPassword} className="fas fa-eye-slash"></i>
            ) : (
              <i onClick={MostrarPassword} className="fas fa-eye"></i>
            )}
          </div>
          {/*<div className="checkbok-login">
            <input type="checkbox" />
            Recordarme
            </div>*/}
          <button
            className="boton-iniciar-sesion"
            type="submit"
            value="Iniciar Sesión"
          >
            Iniciar Sesión
          </button>
        </form>
      </div>
    </Fragment>
  );
}

export default withRouter(Login);
