import { React, Fragment, useState, useContext, useEffect } from 'react';
import urlAxios from '../config/axios';
import { CRMContext } from '../../context/CRMContext';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

import sertutor2 from '../../images/sertutor2.jpg';
import './SerTutor.css';
function getModalStyle() {
  const top = 45;
  const left = 48;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 450,
    height: 200,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[0],
    padding: theme.spacing(2, 4, 3),
  },
}));
function CambioTutor(props) {
  const { id } = props.match.params;

  const [modalStyle] = useState(getModalStyle);
  const [open, setOpen] = useState(false);

  const classes = useStyles();

  const handleClose = () => {
    setOpen(false);
  };
  const [newerror, setError] = useState(null);
  const [auth] = useContext(CRMContext);
  const tutor = 'tutor';

  const [usuario, datosUsuario] = useState({
    nombre: '',
    email: '',
    apellido: '',
    biografia: '',
    role: tutor,
    tutor: 1,
  });

  useEffect(() => {
    if (auth.token !== '') {
      const consultarAPI = async () => {
        try {
          const usuarioConsulta = await urlAxios.get(`/api/users/${id}`, {
            headers: { authorization: `Bearer ${auth.token}` },
          });

          datosUsuario(usuarioConsulta.data);
        } catch (error) {
          if ((error.response.status = 500)) {
            props.history.push('/tutores');
          }
        }
      };
      consultarAPI();
    } else {
    }
  }, [auth.token, id, props.history]);

  console.log();

  const actualizarState = (e) => {
    datosUsuario({
      ...usuario,
      [e.target.name]: e.target.value,
    });
  };
  console.log(usuario);

  const actualizarUsuario = (e) => {
    e.preventDefault();

    urlAxios
      .put(`/api/users/update/${id}`, usuario)
      .then(() => {
        setOpen(true);
      })

      .catch(function (error) {
        setError(error.response.data.error);
        console.log(newerror);
      });
    localStorage.setItem('role', 'tutor');
  };

  return (
    <Fragment>
      {newerror && (
        <div id="message" className="container-alerta">
          {newerror}
        </div>
      )}
      <Modal open={open} onClose={() => handleClose()}>
        <div style={modalStyle} className={classes.paper}>
          <div className="creado">
            <i className="fas fa-check-circle"></i>
            <h1>Cambio a TUTOR exitoso.</h1>
            <Link to={'/tutores'} className="boton-modal">
              Continuar
            </Link>
          </div>
        </div>
      </Modal>
      <div className="pagina-sertutor">
        <h1>CAMBIATE A TUTOR</h1>
        <p>
          Rellena el siguiente formulario con tus datos personales y
          profesionales, luego de que revisemos tu perfil nos pondremos en
          contacto contigo para que en caso de ser aprobado te pasaremos tus
          contrato que deberás leer detenidamente y luego firmar, bienvenido
          futuro tutor de HACKABLOCK.
        </p>
        <img src={sertutor2} alt="" />

        <form onSubmit={actualizarUsuario}>
          <fieldset className="datos-personales">
            <legend> DATOS PERSONALES</legend>
            <div className="datos-personales-blocks">
              <div className="form-1">
                <label for="nombre">
                  Nombre:
                  <input
                    name="nombre"
                    type="text"
                    onChange={actualizarState}
                    value={usuario.nombre}
                  />
                </label>
                <label for="email">
                  Correo electrónico:
                  <input
                    name="email"
                    type="email"
                    onChange={actualizarState}
                    value={usuario.email}
                  />
                </label>
                <label for="fecha_nacimiento">
                  Fecha de nacimiento:
                  <input
                    name="fecha_nacimiento"
                    type="date"
                    onChange={actualizarState}
                  />
                </label>
                <div className="form-2">
                  <label for="telefono">
                    Teléfono:
                    <input
                      name="telefono"
                      type="tel"
                      onChange={actualizarState}
                    />
                  </label>
                  <label for="ciudad">
                    Ciudad:
                    <input
                      name="ciudad"
                      type="text"
                      onChange={actualizarState}
                    />
                  </label>
                </div>
              </div>
              <div className="form-1">
                <label for="apellido">
                  Apellido:
                  <input
                    name="apellido"
                    type="text"
                    onChange={actualizarState}
                    value={usuario.apellido}
                  />
                </label>
                <label for="password">
                  Contraseña:
                  <input
                    name="password"
                    disabled
                    type="password"
                    onChange={actualizarState}
                  />
                </label>
                <label for="direccion">
                  Dirección:
                  <input
                    name="direccion"
                    type="text"
                    onChange={actualizarState}
                  />
                </label>

                <div className="form-2">
                  <label className="label-provincia" for="provincia">
                    Provincia:
                    <select
                      className="provincia"
                      name="provincia"
                      onChange={actualizarState}
                    >
                      <option value="">Elige Provincia</option>
                      <option value="Álava/Araba">Álava/Araba</option>
                      <option value="Albacete">Albacete</option>
                      <option value="Alicante">Alicante</option>
                      <option value="Almería">Almería</option>
                      <option value="Asturias">Asturias</option>
                      <option value="Ávila">Ávila</option>
                      <option value="Badajoz">Badajoz</option>
                      <option value="Baleares">Baleares</option>
                      <option value="Barcelona">Barcelona</option>
                      <option value="Burgos">Burgos</option>
                      <option value="Cáceres">Cáceres</option>
                      <option value="Cádiz">Cádiz</option>
                      <option value="Cantabria">Cantabria</option>
                      <option value="Castellón">Castellón</option>
                      <option value="Ceuta">Ceuta</option>
                      <option value="Ciudad Real">Ciudad Real</option>
                      <option value="Córdoba">Córdoba</option>
                      <option value="Cuenca">Cuenca</option>
                      <option value="Gerona/Girona">Gerona/Girona</option>
                      <option value="Granada">Granada</option>
                      <option value="Guadalajara">Guadalajara</option>
                      <option value="Guipúzcoa/Gipuzkoa">
                        Guipúzcoa/Gipuzkoa
                      </option>
                      <option value="Huelva">Huelva</option>
                      <option value="Huesca">Huesca</option>
                      <option value="Jaén">Jaén</option>
                      <option value="La Coruña/A Coruña">
                        La Coruña/A Coruña
                      </option>
                      <option value="La Rioja">La Rioja</option>
                      <option value="Las Palmas">Las Palmas</option>
                      <option value="León">León</option>
                      <option value="Lérida/Lleida">Lérida/Lleida</option>
                      <option value="Lugo">Lugo</option>
                      <option value="Madrid">Madrid</option>
                      <option value="Málaga">Málaga</option>
                      <option value="Melilla">Melilla</option>
                      <option value="Murcia">Murcia</option>
                      <option value="Navarra">Navarra</option>
                      <option value="Orense/Ourense">Orense/Ourense</option>
                      <option value="Palencia">Palencia</option>
                      <option value="Pontevedra">Pontevedra</option>
                      <option value="Salamanca">Salamanca</option>
                      <option value="Segovia">Segovia</option>
                      <option value="Sevilla">Sevilla</option>
                      <option value="Soria">Soria</option>
                      <option value="Tarragona">Tarragona</option>
                      <option value="Tenerife">Tenerife</option>
                      <option value="Teruel">Teruel</option>
                      <option value="Toledo">Toledo</option>
                      <option value="Valencia">Valencia</option>
                      <option value="Valladolid">Valladolid</option>
                      <option value="Vizcaya/Bizkaia">Vizcaya/Bizkaia</option>
                      <option value="Zamora">Zamora</option>
                      <option value="Zaragoza">Zaragoza</option>
                    </select>
                  </label>
                  <label for="codigo_postal">
                    Código postal:
                    <input
                      name="codigo_postal"
                      type="number"
                      onChange={actualizarState}
                    />
                  </label>
                </div>
              </div>
            </div>
          </fieldset>
          <fieldset className="datos-personales">
            <legend>FORMACIÓN ACADÉMICA</legend>
            <div className="datos-personales-blocks">
              <div className="form-1">
                <label for="profesion">
                  Profesion:
                  <input
                    name="profesion"
                    type="text"
                    onChange={actualizarState}
                  />
                </label>
                <label for="titulo">
                  Titulo:
                  <input name="titulo" type="text" onChange={actualizarState} />
                </label>
              </div>
              <div className="form-1">
                <label for="masterado">
                  Masterado:
                  <input
                    name="masterado"
                    type="text"
                    onChange={actualizarState}
                  />
                </label>
                <label for="doctorado">
                  Doctorado:
                  <input
                    name="doctorado"
                    type="text"
                    onChange={actualizarState}
                  />
                </label>
              </div>
            </div>
          </fieldset>
          <fieldset className="perfil-tutor">
            <legend>PERFIL TUTOR</legend>
            <div className="perfil-tutor-block">
              <label for="biografia">
                Biografia:
                <textarea
                  name="biografia"
                  className="form-biografia"
                  type="text"
                  onChange={actualizarState}
                  value={usuario.biografia}
                />
              </label>
              <div className="categorias-tutor">
                <span>Escoge una o varias categorías:</span>
                <div className="categorias-sub-tutor">
                  <div>
                    <input
                      name="categoria_blockchain"
                      type="checkbox"
                      value="1"
                      onChange={actualizarState}
                    />
                    <label for="categoria_blockchain">Blockchain</label>
                  </div>
                  <div>
                    <input
                      name="categoria_bitcoin"
                      type="checkbox"
                      value="1"
                      onChange={actualizarState}
                    />
                    <label for="categoria_bitcoin">Bitcoin</label>
                  </div>
                  <div>
                    <input
                      name="categoria_crypto"
                      type="checkbox"
                      value="1"
                      onChange={actualizarState}
                    />
                    <label for="categoria_crypto">Crypto</label>
                  </div>
                  <div>
                    <input
                      name="categoria_ethereum"
                      type="checkbox"
                      value="1"
                      onChange={actualizarState}
                    />
                    <label for="categoria_ethereum">Ethereum</label>
                  </div>
                  <div>
                    <input
                      name="categoria_hyperledger"
                      type="checkbox"
                      value="1"
                      onChange={actualizarState}
                    />
                    <label for="categoria_hyperledger">Hyperledger</label>
                  </div>
                </div>
              </div>
              <div className="precio-tutor">
                <span>Selecciona tu rango de precios por hora(€):</span>
                <div className="categorias-sub-tutor">
                  <div>
                    <input
                      name="precio"
                      type="radio"
                      value="1"
                      onChange={actualizarState}
                    />
                    <label for="precio_hora">15 a 30</label>
                  </div>
                  <div>
                    <input
                      type="radio"
                      name="precio_hora"
                      value="2"
                      onChange={actualizarState}
                    />
                    <label for="categoria_bitcoin">35 a 50</label>
                  </div>
                  <div>
                    <input
                      type="radio"
                      name="precio_hora"
                      value="3"
                      onChange={actualizarState}
                    />
                    <label for="categoria_bitcoin">55 a 100</label>
                  </div>
                </div>
              </div>
              <label for="cuenta_pago" className="form-1">
                Cuenta de Pago Paypal:
                <input
                  name="cuenta_pago"
                  type="text"
                  onChange={actualizarState}
                />
              </label>
            </div>
          </fieldset>
          <fieldset className="perfil-tutor">
            <legend>DOCUMENTOS SUSTENTATORIOS</legend>
            <div className="documentos">
              <span>
                Adjunta los documentos que sustenten la información descrita.
                <br></br>
                Archivos en formato: (pdf, jpg, png) y con un peso máximo de
                10MB.
              </span>
              <button className="boton-buscar">BUSCAR ARCHIVOS</button>
              <span>
                <input type="checkbox" />
                Estoy de acuerdo con los{' '}
                <span className="negrita">términos y condiciones</span>
              </span>
              <button
                className="boton-enviar"
                value={((usuario.tutor = 1), (usuario.role = 'tutor'))}
              >
                SER TUTOR
              </button>
            </div>
          </fieldset>
        </form>
      </div>
    </Fragment>
  );
}

export default withRouter(CambioTutor);
