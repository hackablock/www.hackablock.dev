import React, { Fragment, useState } from 'react';
import urlAxios from '../config/axios';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import './RegistroUsuario.css';

function getModalStyle() {
  const top = 45;
  const left = 48;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 450,
    height: 200,
    backgroundColor: theme.palette.background.paper,
    /*border: '2px solid #000',*/
    boxShadow: theme.shadows[0],
    padding: theme.spacing(2, 4, 3),
  },
}));

function RegistroUsuario(props) {
  const [modalStyle] = useState(getModalStyle);
  const [open, setOpen] = useState(false);

  const classes = useStyles();

  const handleClose = () => {
    setOpen(false);
  };

  const [error, setError] = useState(null);
  const student = 'student';
  const avatardefault = 'avatardefault.png';
  const [mostrarPass, setMostrarPass] = useState(false);

  const [usuario, guardarUsuario] = useState({
    nombre: '',
    email: '',
    password: '',
    role: student,
    tutor: 0,
    avatar: avatardefault,
  });

  const MostrarPassword = () => {
    setMostrarPass(mostrarPass ? false : true);
  };

  const actualizarState = (e) => {
    guardarUsuario({
      ...usuario,
      [e.target.name]: e.target.value,
    });
  };

  const agregarUsuario = (e) => {
    e.preventDefault();
    setError(null);

    urlAxios
      .post('api/users/register', usuario)
      .then(() => {
        setOpen(true);
      })
      .catch(function (error) {
        setError(error.response.data.error);
      });
  };

  const cerrarError = () => {
    setError(null);
  };

  return (
    <Fragment>
      {error && (
        <div id="message" className="container-alerta">
          {error}
          <i class="far fa-window-close" onClick={cerrarError}></i>
        </div>
      )}
      <Modal open={open} onClose={() => handleClose()}>
        <div style={modalStyle} className={classes.paper}>
          <div className="creado">
            <i className="fas fa-check-circle"></i>
            <h1>Usuario creado con éxito.</h1>
            <Link to={'/usuarios/iniciar-sesion'} className="boton-modal">
              Login
            </Link>
          </div>
        </div>
      </Modal>
      <div className="container-registro">
        <form onSubmit={agregarUsuario}>
          <div className="titulo-form-crear">
            ¡Crea una cuenta en HACKABLOCK!
          </div>
          <div className="font-user">
            <input
              id="nombre"
              type="text"
              placeholder="Nombre de Usuario"
              name="nombre"
              onChange={actualizarState}
            />
            <i className="fas fa-user"></i>
          </div>

          <div className="font-user">
            <input
              type="text"
              placeholder="Email de Usuario"
              name="email"
              onChange={actualizarState}
            />
            <i className="fas fa-envelope"></i>
          </div>

          <div className="font-user">
            <input
              type={mostrarPass ? 'text' : 'password'}
              placeholder="Contraseña"
              name="password"
              required
              onChange={actualizarState}
            />
            <i className="fas fa-key"></i>
            {mostrarPass === false ? (
              <i onClick={MostrarPassword} className="fas fa-eye-slash"></i>
            ) : (
              <i onClick={MostrarPassword} className="fas fa-eye"></i>
            )}
          </div>
          <div className="contraseña-terminos">
            <div className="checkbox-link">
              <input type="checkbox" />
              Acepto los <strong>términos y condiciones</strong>
            </div>

            <div className="contraseña-olvidada">
              ¿Contraseña
              <br /> olvidada?
            </div>
          </div>

          <div>
            <button
              type="submit"
              className="boton-crear-cuenta"
              value="Crear Cuenta"
              onClick={agregarUsuario}
            >
              Crear Cuenta
            </button>
          </div>
          <div className="registrado-iniciarsesion">
            <div>¿Estás registrado?</div>
            <Link
              to={'/usuarios/iniciar-sesion'}
              className="link-iniciarsesion"
            >
              Iniciar sesión
            </Link>
          </div>
        </form>
      </div>
    </Fragment>
  );
}

export default RegistroUsuario;
