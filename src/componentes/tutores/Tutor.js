import React from 'react';

import { Link } from 'react-router-dom';
import './tutores.css';

function Tutor({ tutor }) {
  const {
    id,
    nombre,
    categoria,
    valoracion,
    apellido,
    biografia,
    foto,
    clases,
    pais,
    profesion,
    precio,
  } = tutor;

  return (
    <Link to={`/tutores/${id}`} className="entrada-tutor">
      <div className="imagen-tutor">
        <img src={`http://localhost:5000/${foto}`} alt="" />
      </div>
      <div className="info-tutor">
        <span className="nombre-tutor">
          {nombre} {apellido}
        </span>
        <span className="profesion-tutor">{profesion}</span>
        <div className="valoracion-tutor">
          {Array.from({ length: valoracion }, (_, i) => (
            <i key={i} className="fa fa-star amarilla" aria-hidden="true"></i>
          ))}
        </div>
        <div className="tags-tutor">
          <div className="blocks">{categoria}</div>
          <div className="blocks">€{precio}/Hora</div>
          <div className="blocks">{pais}</div>
          <div className="blocks">{clases} Clases</div>
        </div>
        <div className="bio">{biografia}</div>
      </div>
    </Link>
  );
}

export default Tutor;
