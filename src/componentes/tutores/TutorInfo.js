import urlAxios from '../config/axios';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';

import './tutores.css';

function Tutor() {
  const { id } = useParams();
  const [tutor, guardarTutor] = useState({});

  const consultarAPI = async () => {
    const tutorConsulta = await urlAxios.get(`/api/teachers/${id}`);
    guardarTutor(tutorConsulta.data);
  };

  useEffect(() => {
    consultarAPI();
  }, [id]);

  const {
    nombre,
    categoria,
    valoracion,
    apellido,
    biografia,
    clases,
    pais,
    profesion,
    precio,
    avatar,
  } = tutor;

  return (
    <article className="contenedor-tutor-info">
      <div className="contenedor-izquierdo-tutor-info">
        <iframe
          src="https://player.vimeo.com/video/272967094?h=a94f1fe42d&autoplay=1&color=cb2844&title=0&byline=0&portrait=0"
          width="640"
          height="360"
          frameBorder="0"
          allow="autoplay; fullscreen; picture-in-picture"
          allowFullScreen
        ></iframe>

        <div className="info-tutor-info">
          <div className="imagen-tutor-info">
            <img src={`http://localhost:5000/${avatar}`} alt="" />
          </div>
          <div className="datos-tutor-info">
            <span className="nombre-tutor-info">
              {nombre} {apellido}
            </span>
            <span className="profesion-tutor-info">{profesion}</span>
            <div className="valoracion-tutor">
              {Array.from({ length: valoracion }, (_, i) => (
                <i
                  key={i}
                  className="fa fa-star amarilla"
                  aria-hidden="true"
                ></i>
              ))}
            </div>
            <div className="tags-tutor">
              <div className="blocks">{categoria}</div>
              <div className="blocks">€{precio}/Hora</div>
              <div className="blocks">{pais}</div>
              <div className="blocks">{clases} Clases</div>
            </div>
          </div>
        </div>
        <div className="bio-info">
          <p>
            <strong>Sobre Mí</strong>
          </p>
          <span>{biografia}</span>
        </div>
        <div className="bio-info">
          <p>
            <strong>Experiencia</strong>
          </p>
          <span>{biografia}</span>
        </div>
        <div className="bio-agenda">
          <h1>AGENDA DISPONIBLE</h1>
          <div className="dias-info-tutor">
            <div className="dia-individual">
              <span>Lunes</span>
              <span>19</span>
              <p>12:45</p>
            </div>
            <div className="dia-individual">
              <span>Martes</span>
              <span>20</span>
              <p>16:00</p>
            </div>
            <div className="dia-individual">
              <span>Miércoles</span>
              <span>21</span>
              <p>13:30</p>
            </div>
            <div className="dia-individual">
              <span>Jueves</span>
              <span>22</span>
              <p>16:15</p>
            </div>
            <div className="dia-individual">
              <span>Viernes</span>
              <span>23</span>
              <p>08:30</p>
            </div>
            <div className="dia-individual">
              <span>Sábado</span>
              <span>24</span>
              <p>15:00</p>
            </div>
          </div>
        </div>
      </div>
      <div className="contenedor-derecho-tutor-info">
        <div className="clases-prueba">
          <h1>Clase de prueba</h1>
          <div className="clase-prueba-gratis">
            30min
            <strong>Gratis</strong>
          </div>
          <div className="boton-reserva-gratis">Reservar</div>
        </div>
        <div className="clases-particulares">
          <h1>Particulares</h1>
          <div className="clase-prueba-gratis">
            1 Clase
            <strong>36€</strong>
          </div>
          <div className="clase-prueba-gratis">
            5 Clases
            <strong>72€</strong>
          </div>
          <div className="clase-prueba-gratis">
            10 Clases
            <strong>85€</strong>
          </div>
          <div className="boton-reserva-gratis">Reservar</div>
        </div>
      </div>
    </article>
  );
}

export default Tutor;
