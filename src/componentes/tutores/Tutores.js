import React, { useEffect, useState, Fragment } from 'react';
import urlAxios from '../config/axios';
import Tutor from './Tutor';
import { withRouter } from 'react-router-dom';

function Tutores() {
  const [tutores, guardarTutores] = useState([]);
  const [ordenar, guardarOrdenar] = useState({});

  useEffect(() => {
    const consultarAPI = async () => {
      const tutoresConsulta = await urlAxios.get('/api/teachers');

      guardarTutores(tutoresConsulta.data);
    };
    consultarAPI();
  }, []);

  const ordenarPor = (e) => {
    guardarOrdenar({
      ...ordenar,
      [e.target.name]: e.target.value,
    });
  };

  const list = tutores;
  const x = ordenar.value;

  if (Number(x) === 0) {
    list.sort((a, b) => (a.id > b.id ? 1 : -1));
  }
  if (Number(x) === 1) {
    list.sort((a, b) => (b.valoracion > a.valoracion ? 1 : -1));
  }
  if (Number(x) === 2) {
    list.sort((a, b) => (b.precio > a.precio ? 1 : -1));
  }
  if (Number(x) === 3) {
    list.sort((a, b) => (a.precio > b.precio ? 1 : -1));
  }
  if (Number(x) === 4) {
    list.sort((a, b) => (a.categoria > b.categoria ? 1 : -1));
  }
  window.onbeforeunload = function () {
    window.scrollTo(0, 0);
  };

  return (
    <Fragment>
      <main className="pagina-tutores">
        <div className="tutor-filtros-top">
          <div className="tutor-ordenar-por">
            <p>Ordenar por:</p>
            <select name="value" onChange={ordenarPor}>
              <option name="value" value="0">
                Seleccionar:
              </option>
              <option name="value" value="1">
                Valoración (+) (-)
              </option>
              <option name="value" value="2">
                Precio (+) (-)
              </option>
              <option name="value" value="3">
                Precio (-) (+)
              </option>
              <option name="value" value="4">
                Categoría (A a Z)
              </option>
            </select>
          </div>
        </div>
        <ul className="contenido-tutores">
          {tutores.map((tutor) => (
            <Tutor key={tutor.id} tutor={tutor} />
          ))}
        </ul>
      </main>
    </Fragment>
  );
}

export default withRouter(Tutores);
