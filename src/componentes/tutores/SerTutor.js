import { React, Fragment, useState, useEffect } from 'react';
import urlAxios from '../config/axios';
import sertutor1 from '../../images/sertutor1.jpeg';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import './SerTutor.css';

function getModalStyle() {
  const top = 45;
  const left = 48;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 450,
    height: 200,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[0],
    padding: theme.spacing(2, 4, 3),
  },
}));

function SerTutor(props) {
  const [modalStyle] = useState(getModalStyle);
  const [open, setOpen] = useState(false);

  const classes = useStyles();

  const handleClose = () => {
    setOpen(false);
  };
  const [newerror, setError] = useState(null);
  const [tutor, guardarTutor] = useState({
    nombre: '',
    email: '',
    password: '',
    fecha_nacimiento: '',
    telefono: '',
    ciudad: '',
    apellido: '',
    direccion: '',
    provincia: '',
    codigo_postal: '',
    profesion: '',
    tutor: '1',
    role: 'tutor',
    avatar: 'avatardefault.png',
  });

  const actualizarState = (e) => {
    guardarTutor({
      ...tutor,
      [e.target.name]: e.target.value,
    });
  };

  const agregarTutor = (e) => {
    e.preventDefault();
    setError(null);

    urlAxios
      .post('api/users/register', tutor)
      .then(() => {
        setOpen(true);
      })
      .catch(function (error) {
        setError(error.response.data.error);
      });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <Fragment>
      {newerror && (
        <div id="message" className="container-alerta">
          {newerror}
        </div>
      )}
      <Modal open={open} onClose={() => handleClose()}>
        <div style={modalStyle} className={classes.paper}>
          <div className="creado">
            <i className="fas fa-check-circle"></i>
            <h1>TUTOR creado con éxito.</h1>
            <Link to={'/usuarios/iniciar-sesion'} className="boton-modal">
              Login
            </Link>
          </div>
        </div>
      </Modal>
      <div className="pagina-sertutor">
        <h1>REGÍSTRATE COMO TUTOR</h1>
        <p>
          Rellena el siguiente formulario con tus datos personales y
          profesionales, luego de que revisemos tu perfil nos pondremos en
          contacto contigo para que en caso de ser aprobado te pasaremos tus
          contrato que deberás leer detenidamente y luego firmar, bienvenido
          futuro tutor de HACKABLOCK.
        </p>
        <img src={sertutor1} alt="" />

        <form onSubmit={agregarTutor}>
          <fieldset className="datos-personales">
            <legend> DATOS PERSONALES</legend>
            <div className="datos-personales-blocks">
              <div className="form-1">
                <label>
                  Nombre:
                  <input name="nombre" type="text" onChange={actualizarState} />
                </label>
                <label>
                  Correo electrónico:
                  <input name="email" type="email" onChange={actualizarState} />
                </label>
                <label>
                  Fecha de nacimiento:
                  <input
                    name="fecha_nacimiento"
                    type="date"
                    onChange={actualizarState}
                  />
                </label>
                <div className="form-2">
                  <label>
                    Teléfono:
                    <input
                      name="telefono"
                      type="tel"
                      onChange={actualizarState}
                    />
                  </label>
                  <label>
                    Ciudad:
                    <input
                      name="ciudad"
                      type="text"
                      onChange={actualizarState}
                    />
                  </label>
                </div>
              </div>
              <div className="form-1">
                <label>
                  Apellido:
                  <input
                    name="apellido"
                    type="text"
                    onChange={actualizarState}
                  />
                </label>
                <label>
                  Contraseña:
                  <input
                    name="password"
                    type="password"
                    onChange={actualizarState}
                  />
                </label>
                <label>
                  Dirección:
                  <input
                    name="direccion"
                    type="text"
                    onChange={actualizarState}
                  />
                </label>

                <div className="form-2">
                  <label className="label-pais">
                    País:
                    <select
                      className="pais"
                      name="pais"
                      onChange={actualizarState}
                    >
                      <option value="">Seleccionar</option>
                      <option value="Argentina">Argentina</option>
                      <option value="Bolivia">Bolivia</option>
                      <option value="Brazil">Brazil</option>
                      <option value="Chile">Chile</option>
                      <option value="Colombia">Colombia</option>
                      <option value="Costa Rica">Costa Rica</option>
                      <option value="Cuba">Cuba</option>
                      <option value="Curaco">Curacao</option>
                      <option value="East Timor">East Timor</option>
                      <option value="Ecuador">Ecuador</option>
                      <option value="El Salvador">El Salvador</option>
                      <option value="España">España</option>
                      <option value="Guatemala">Guatemala</option>
                      <option value="Mexico">Mexico</option>
                      <option value="Nicaragua">Nicaragua</option>
                      <option value="Panama">Panama</option>
                      <option value="Paraguay">Paraguay</option>
                      <option value="Peru">Peru</option>
                      <option value="Portugal">Portugal</option>
                      <option value="Puerto Rico">Puerto Rico</option>
                      <option value="Republica Domenicana">
                        Republica Domenicana
                      </option>
                      <option value="United States of America">
                        United States of America
                      </option>
                      <option value="Uraguay">Uruguay</option>
                      <option value="Venezuela">Venezuela</option>
                    </select>
                  </label>
                  <label>
                    Código postal:
                    <input
                      name="codigo_postal"
                      type="number"
                      onChange={actualizarState}
                    />
                  </label>
                </div>
              </div>
            </div>
          </fieldset>
          <fieldset className="datos-personales">
            <legend>FORMACIÓN ACADÉMICA</legend>
            <div className="datos-personales-blocks">
              <div className="form-1">
                <label>
                  Profesion:
                  <input
                    name="profesion"
                    type="text"
                    onChange={actualizarState}
                  />
                </label>
                <label>
                  Titulo:
                  <input name="titulo" type="text" onChange={actualizarState} />
                </label>
              </div>
              <div className="form-1">
                <label>
                  Masterado:
                  <input
                    name="masterado"
                    type="text"
                    onChange={actualizarState}
                  />
                </label>
                <label>
                  Doctorado:
                  <input
                    name="doctorado"
                    type="text"
                    onChange={actualizarState}
                  />
                </label>
              </div>
            </div>
          </fieldset>
          <fieldset className="perfil-tutor">
            <legend>PERFIL TUTOR</legend>
            <div className="perfil-tutor-block">
              <label>
                Biografia:
                <textarea
                  name="biografia"
                  className="form-biografia"
                  type="text"
                  onChange={actualizarState}
                />
              </label>
              <div className="categorias-tutor">
                <span>Escoge una o varias categorías:</span>
                <div className="categorias-sub-tutor">
                  <div>
                    <input
                      name="categoria_blockchain"
                      type="checkbox"
                      value="1"
                      onChange={actualizarState}
                    />
                    <label>Blockchain</label>
                  </div>
                  <div>
                    <input
                      name="categoria_bitcoin"
                      type="checkbox"
                      value="1"
                      onChange={actualizarState}
                    />
                    <label>Bitcoin</label>
                  </div>
                  <div>
                    <input
                      name="categoria_crypto"
                      type="checkbox"
                      value="1"
                      onChange={actualizarState}
                    />
                    <label>Crypto</label>
                  </div>
                  <div>
                    <input
                      name="categoria_ethereum"
                      type="checkbox"
                      value="1"
                      onChange={actualizarState}
                    />
                    <label>Ethereum</label>
                  </div>
                  <div>
                    <input
                      name="categoria_hyperledger"
                      type="checkbox"
                      value="1"
                      onChange={actualizarState}
                    />
                    <label>Hyperledger</label>
                  </div>
                </div>
              </div>
              <div className="precio-tutor">
                <span>Selecciona tu rango de precios por hora(€):</span>
                <div className="categorias-sub-tutor">
                  <div>
                    <input
                      name="precio"
                      type="radio"
                      value="1"
                      onChange={actualizarState}
                    />
                    <label>15 a 30</label>
                  </div>
                  <div>
                    <input
                      type="radio"
                      name="precio_hora"
                      value="2"
                      onChange={actualizarState}
                    />
                    <label>35 a 50</label>
                  </div>
                  <div>
                    <input
                      type="radio"
                      name="precio_hora"
                      value="3"
                      onChange={actualizarState}
                    />
                    <label>55 a 100</label>
                  </div>
                </div>
              </div>
              <label className="form-1">
                Cuenta de Pago Paypal:
                <input
                  name="cuenta_pago"
                  type="text"
                  onChange={actualizarState}
                />
              </label>
            </div>
          </fieldset>
          <fieldset className="perfil-tutor">
            <legend>DOCUMENTOS SUSTENTATORIOS</legend>
            <div className="documentos">
              <span>
                Adjunta los documentos que sustenten la información descrita.
                <br></br>
                Archivos en formato: (pdf, jpg, png) y con un peso máximo de
                10MB.
              </span>
              <button className="boton-buscar">BUSCAR ARCHIVOS</button>
              <span>
                <input type="checkbox" />
                Estoy de acuerdo con los{' '}
                <span className="negrita">términos y condiciones</span>
              </span>
              <button className="boton-enviar">ENVIAR</button>
            </div>
          </fieldset>
        </form>
      </div>
    </Fragment>
  );
}

export default SerTutor;
