import React from 'react';

import { Link } from 'react-router-dom';
import './tutores.css';

function TutorLanding({ tutor }) {
  const { id, nombre, apellido, foto, profesion } = tutor;

  return (
    <Link to={`/cursos/${id}`}>
      <div className="entrada-tutor-landing">
        <div className="imagen-tutor-landing">
          <img src={`http://localhost:5000/${foto}`} alt="" />
        </div>

        <div className="info-tutor-landing">
          <p>
            <strong>
              {nombre} {apellido}
            </strong>
            <br></br>

            {profesion}
          </p>
        </div>
      </div>
    </Link>
  );
}

export default TutorLanding;
