import React, { Fragment, useState, useContext, useEffect } from 'react';
import urlAxios from '../config/axios';
import { CRMContext } from '../../context/CRMContext';
import { withRouter } from 'react-router-dom';
import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';
import './EditarUsuario.css';

function getModalStyle() {
  const top = 44;
  const left = 48.5;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 450,
    height: 200,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[0],
    padding: theme.spacing(2, 4, 3),
  },
}));

function EditarUsuario(props) {
  const [modalStyle] = useState(getModalStyle);
  const [open, setOpen] = useState(false);
  const classes = useStyles();

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const { id } = props.match.params;

  const [auth, guardarAuth] = useContext(CRMContext);
  const [error, setError] = useState(null);
  const [cambios, setCambios] = useState(null);

  const [usuario, datosUsuario] = useState({
    nombre: '',
    email: '',
    apellido: '',
    biografia: '',
    sitio_web: '',
    twitter: '',
    facebok: '',
    linkedin: '',
    youtube: '',
  });

  useEffect(() => {
    if (auth.token !== '') {
      const consultarAPI = async () => {
        try {
          const usuarioConsulta = await urlAxios.get(`/api/users/${id}`, {
            headers: { authorization: `Bearer ${auth.token}` },
          });

          datosUsuario(usuarioConsulta.data);
        } catch (error) {
          // Error con autohorizacion
          if ((error.response.status = 500)) {
            props.history.push('/usuarios/iniciar-sesion');
          }
        }
      };

      consultarAPI();
    } else {
    }
  }, [auth.token, id, props.history]);

  const actualizarState = (e) => {
    datosUsuario({
      ...usuario,
      [e.target.name]: e.target.value,
    });
  };

  const actualizarUsuario = (e) => {
    e.preventDefault();
    setError(null);

    urlAxios
      .put(`/api/users/update/${id}`, usuario)
      .then(() => {
        setCambios(true);
        setTimeout(() => setCambios(false), 5000);
      })

      .catch(function (error) {
        setError(error.response.data.error);
      });
  };

  const eliminarUsuario = (id) => {
    urlAxios.delete(`/api/users/delete/${id}`).then(() => {
      guardarAuth({
        token: '',
        auth: false,
      });

      localStorage.clear();
      props.history.push('/usuarios/registro');
    });
  };
  const cerrarError = () => {
    setError(null);
  };
  const cerrarCambio = () => {
    setCambios(null);
  };

  const { avatar } = usuario;
  return (
    <Fragment>
      {cambios && (
        <div id="message" className="container-alerta-editar-estudiante-ok">
          Cambios guardados correctamente
          <i className="far fa-window-close" onClick={cerrarCambio}></i>
        </div>
      )}
      {error && (
        <div id="message" className="container-alerta-editar-estudiante">
          {error}
          <i class="far fa-window-close" onClick={cerrarError}></i>
        </div>
      )}
      <Modal open={open} onClose={() => handleClose()}>
        <div style={modalStyle} className={classes.paper}>
          <div className="cambios">
            <i className="fas fa-exclamation-triangle"></i>
            <h1>¿Estás seguro de eliminar la cuenta?</h1>
            <div className="si-no">
              <button onClick={handleClose} className="no">
                No estoy seguro
              </button>
              <button
                className="si"
                onClick={() => {
                  eliminarUsuario(id);
                }}
              >
                Si, eliminar la cuenta
              </button>
            </div>
          </div>
        </div>
      </Modal>

      <div className="container-editar">
        <div className="container-avatar">
          <img src={`http://localhost:5000/${avatar}`} alt="" />
        </div>
        <form onSubmit={actualizarUsuario}>
          <h1 className="titulo-perfil-estudiante">Perfil Tutor</h1>
          <div className="campos-izquierdo-derecho">
            <div className="container-izquierdo">
              <div className="campo-perfil-izquierdo">
                <label>Nombre:</label>
                <input
                  type="text"
                  name="nombre"
                  onChange={actualizarState}
                  value={usuario.nombre}
                />
              </div>

              <div className="campo-perfil-izquierdo">
                <label>Apellido:</label>
                <input
                  type="text"
                  name="apellido"
                  onChange={actualizarState}
                  value={usuario.apellido}
                />
              </div>
              <div className="campo-perfil-izquierdo">
                <label>Email:</label>
                <input
                  type="text"
                  name="email"
                  onChange={actualizarState}
                  value={usuario.email}
                />
              </div>
              <div className="campo-perfil-izquierdo">
                <label>Biografía:</label>
                <textarea
                  className="biografia"
                  type="text"
                  name="biografia"
                  onChange={actualizarState}
                  value={usuario.biografia}
                ></textarea>
              </div>
              <div>
                <input type="submit" value="Guardar Cambios" />
              </div>
            </div>
            <div className="container-derecho">
              <div className="campo-perfil-izquierdo">
                <label>Sitio web:</label>
                <input
                  type="text"
                  name="sitio_web"
                  onChange={actualizarState}
                  value={usuario.sitio_web}
                />
              </div>

              <div className="campo-perfil-izquierdo">
                <label>Twietter</label>
                <input
                  type="text"
                  name="twitter"
                  onChange={actualizarState}
                  value={usuario.twitter}
                />
              </div>
              <div className="campo-perfil-izquierdo">
                <label>Facebok:</label>
                <input
                  type="text"
                  name="facebok"
                  onChange={actualizarState}
                  value={usuario.facebok}
                />
              </div>
              <div className="campo-perfil-izquierdo">
                <label>Lindedin:</label>
                <input
                  type="text"
                  name="linkedin"
                  onChange={actualizarState}
                  value={usuario.linkedin}
                />
              </div>
              <div className="campo-perfil-izquierdo">
                <label>Youtube:</label>
                <input
                  type="text"
                  name="youtube"
                  onChange={actualizarState}
                  value={usuario.youtube}
                />
              </div>
              <div>
                <button
                  type="button"
                  className=""
                  value="Eliminar Cuenta"
                  onClick={handleOpen}
                >
                  Eliminar Usuario
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </Fragment>
  );
}

export default withRouter(EditarUsuario);
