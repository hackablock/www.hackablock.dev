import React, { useContext } from 'react';
import { CRMContext } from '../../context/CRMContext';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';

const Header = () => {
  const [auth] = useContext(CRMContext);

  return (
    <header>
      {!auth.auth ? (
        <div className="loginbar">
          <div>
            <Link
              to={'/usuarios/iniciar-sesion'}
              className="login-registro-link"
            >
              Login
            </Link>
          </div>
          <div className="linea-vertical"></div>
          <div>
            <Link to={'/usuarios/registro'} className="login-registro-link">
              Registrarse
            </Link>
          </div>
        </div>
      ) : (
        <div className="loginbar2"></div>
      )}
    </header>
  );
};

export default withRouter(Header);
