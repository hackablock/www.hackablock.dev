import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { CRMContext } from '../../context/CRMContext';

const Navegacion = () => {
  const userId = localStorage.getItem('userId');
  const [auth] = useContext(CRMContext);

  return (
    <div className="">
      <nav className="menu">
        <Link to={'/'} className="link-menu">
          Inicio
        </Link>
        <Link to={'/tutores'} className="link-menu">
          Tutores
        </Link>
        <Link to={'/cursos'} className="link-menu">
          Cursos
        </Link>

        {auth.auth ? (
          <Link to={`/usuarios/cambio-tutor/${userId}`} className="link-menu">
            ¿Quieres ser tutor?
          </Link>
        ) : (
          <Link to={'/ser-tutor'} className="link-menu">
            ¿Quieres ser tutor?
          </Link>
        )}
      </nav>
    </div>
  );
};

export default Navegacion;
