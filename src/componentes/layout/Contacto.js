import React from 'react';
import './Contacto.css';

function Contacto() {
  return (
    <div className="contenedor-contacto">
      <div className="contacto-align">
        <div className="iconos-contacto">
          <i className="fas fa-phone"></i>
        </div>
        <div className="movil">+34 672 23 05 45</div>
      </div>
      <div className="contacto-align">
        <div className="iconos-contacto">
          <i className="fas fa-map-marker-alt"></i>
        </div>
        <div className="movil">Santiago de Compostela</div>
      </div>
      <div className="contacto-align">
        <div className="iconos-contacto">
          <i className="fa fa-envelope"></i>
        </div>
        <div className="movil">info@hackablock.com</div>
      </div>
      <div className="contacto-align">
        <div className="iconos-contacto-horario">
          <i className="fas fa-clock"></i>
        </div>
        <div className="horario">
          Lunes a Viernes.
          <br />
          10:00 - 17:00
        </div>
      </div>
    </div>
  );
}

export default Contacto;
