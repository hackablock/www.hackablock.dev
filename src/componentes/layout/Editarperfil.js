import React from 'react';
import { Link, withRouter } from 'react-router-dom';

function Editarperfil() {
  const userId = localStorage.getItem('userId');
  const roledude = localStorage.getItem('role');
  const condition = 'student';

  return (
    <div>
      {roledude === condition ? (
        <Link to={`/usuarios/editar/${userId}`} className="perfil-cerrarsesion">
          Editar perfil
        </Link>
      ) : (
        <Link to={`/tutores/editar/${userId}`} className="perfil-cerrarsesion">
          Editar perfil
        </Link>
      )}
    </div>
  );
}

export default withRouter(Editarperfil);
