import React, { useContext, useEffect, useState, Fragment } from 'react';
import urlAxios from '../config/axios';
import { CRMContext } from '../../context/CRMContext';
import { withRouter } from 'react-router-dom';
import Navegacion from './Navegacion';
import Editarperfil from './Editarperfil';

const Header = (props) => {
  const [usuario, guardarUsuario] = useState({});
  const [auth, guardarAuth] = useContext(CRMContext);
  const [isShown, setIsShown] = useState(false);
  const userId = localStorage.getItem('userId');

  const consultarAPI = async () => {
    const usuarioConsulta = await urlAxios.get(`/api/users/${userId}`, {
      headers: { authorization: `Bearer ${auth.token}` },
    });
    guardarUsuario(usuarioConsulta.data);
  };

  useEffect(() => {
    consultarAPI();
  }, [userId]);

  const cerrarSesion = () => {
    guardarAuth({
      token: '',
      auth: false,
    });
    localStorage.clear();
    setIsShown(false);
    props.history.push('/usuarios/iniciar-sesion');
  };

  const { avatar } = usuario;
  return (
    <Fragment>
      <header>
        <div className="menu-logo">
          <h1 className="logo-titulo">HACKABLOCK</h1>
          <Navegacion />
          <div className="botones-perfil">
            {auth.auth && (
              <ul className="avatar-vertical">
                <li>
                  <img
                    src={`http://localhost:5000/${avatar}`}
                    alt=""
                    onMouseEnter={() => setIsShown(true)}
                  />
                </li>
                {isShown && (
                  <li
                    className="menu-vertical"
                    onMouseLeave={() => setIsShown(false)}
                  >
                    <div>
                      <Editarperfil />
                    </div>
                    <div className="perfil-cerrarsesion" onClick={cerrarSesion}>
                      Cerrar sesion
                    </div>
                  </li>
                )}
              </ul>
            )}
          </div>
        </div>
      </header>
    </Fragment>
  );
};

export default withRouter(Header);
