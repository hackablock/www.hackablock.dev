import React, { Fragment, useContext } from 'react';
import { Link } from 'react-router-dom';
import { CRMContext } from '../../context/CRMContext';
import './Footer.css';
import Contacto from './Contacto';
import Redesociales from './Redesociales';

function Footer() {
  const userId = localStorage.getItem('userId');
  const roledude = localStorage.getItem('role');

  const [auth] = useContext(CRMContext);
  window.onbeforeunload = function () {
    window.scrollTo(0, 0);
  };

  return (
    <Fragment>
      <footer>
        <div className="footer-contenedor">
          <Contacto />
          <div className="redes-nav-sertutor">
            <div>
              <div className="logo-footer">HACKABLOCK</div>
              <Redesociales />
            </div>
            <div className="menu-footer">
              <Link to={'/'} className="link-menu-footer">
                Inicio
              </Link>
              <Link to={'/tutores'} className="link-menu-footer">
                Tutores
              </Link>
              <Link to={'/cursos'} className="link-menu-footer">
                Cursos
              </Link>
              <Link to={'/contactos'} className="link-menu-footer">
                Contacto
              </Link>
            </div>

            <div className="nav-sertutor">
              <div>
                {auth.auth ? (
                  <Link
                    to={`/usuarios/cambio-tutor/${userId}`}
                    className="link-menu-sertutor"
                  >
                    SER TUTOR
                  </Link>
                ) : (
                  <Link to={'/ser-tutor'} className="link-menu-sertutor">
                    SER TUTOR
                  </Link>
                )}
              </div>
              <div>
                <Link
                  to={'/terminos-y-condiciones'}
                  className="link-terminos-condiciones"
                >
                  Términos y condiciones
                </Link>
              </div>
              <div className="link-copyright">
                <p>© 2021 HACKABLOCK</p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </Fragment>
  );
}

export default Footer;
