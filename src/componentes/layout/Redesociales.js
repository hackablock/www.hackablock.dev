import React from 'react';
import './Redesociales.css';

function Redesociales() {
  return (
    <div className="contenedor-redesociales">
      <div className="circle">
        <i className="fab fa-google-plus-g"></i>
      </div>
      <div className="circle">
        <i className="fab fa-linkedin-in"></i>
      </div>
      <div className="circle">
        <i className="fab fa-github"></i>
      </div>
      <div className="circle">
        <i className="fab fa-facebook-f"></i>
      </div>
      <div className="circle">
        <i className="fab fa-snapchat-ghost"></i>
      </div>
    </div>
  );
}

export default Redesociales;
