import React, { Fragment, useContext } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './componentes/layout/Header';
import Loginbar from './componentes/layout/Loginbar';
import Tutores from './componentes/tutores/Tutores';
import RegistroUsuario from './componentes/tutores/RegistroUsuario';
import EditarUsuario from './componentes/tutores/EditarUsuario';
import Cursos from './componentes/cursos/Cursos';
import Login from './componentes/auth/Login';
import Landing from './componentes/paginas/Landing';
import Footer from './componentes/layout/Footer';
import Editarperfil from './componentes/layout/Editarperfil';
import CursoInfo from './componentes/cursos/CursoInfo';
import TutorInfo from './componentes/tutores/TutorInfo';
import SerTutor from './componentes/tutores/SerTutor';
import EditarTutor from './componentes/tutores/EditarTutor';
import CambioTutor from './componentes/tutores/CambioTutor';

import { CRMContext, CRMProvider } from './context/CRMContext';

function App() {
  const [auth, guardarAuth] = useContext(CRMContext);

  return (
    <Router>
      <Fragment>
        <CRMProvider value={[auth, guardarAuth]}>
          <Loginbar />
          <Header />

          <Switch>
            <Route exact path="/tutores" component={Tutores} />
            <Route
              exact
              path="/usuarios/registro"
              component={RegistroUsuario}
            />
            <Route
              exact
              path="/usuarios/editar/:id"
              component={EditarUsuario}
            />
            <Route
              exact
              path="/usuarios/editar/:id"
              component={EditarUsuario}
            />
            <Route
              exact
              path="/usuarios/cambio-tutor/:id"
              component={CambioTutor}
            />

            <Route exact path="/tutores/editar/:id" component={EditarTutor} />
            <Route exact path="/usuarios/editar/:id" component={Editarperfil} />
            <Route exact path="/" component={Landing} />
            <Route exact path="/cursos" component={Cursos} />
            <Route exact path="/cursos/:id" component={CursoInfo} />
            <Route exact path="/tutores/:id" component={TutorInfo} />
            <Route exact path="/usuarios/iniciar-sesion" component={Login} />
            <Route exact path="/ser-tutor" component={SerTutor} />
            <Route
              exact
              path="/usuarios/cambio-tutor"
              component={CambioTutor}
            />
          </Switch>

          <Footer />
        </CRMProvider>
      </Fragment>
    </Router>
  );
}

export default App;
